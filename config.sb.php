<?php
ini_set("display_errors","on");

return [
    //database
    'database.host'     => '172.25.1.11',
    'database.port'     => 5555,
    'database.dbname'   => 'credit_ty',
    'database.user'     => 'credit_ty',
    'database.password' => 'credit_ty',
    'database.tablepre' => 'pcore_',
    //mongo
    'mongo.host' => 'mongodb://172.25.1.22:27023',
    'mongo.uriOptions' => [
        'username'=>"credit_ty",
        'password'=>"credit_ty"
    ],
    'mongo.driverOptions' => [
    ],
    //cache
    'cache.route.disable' => false,
    //memcached
    'memcached.service'=>[
        ['credit-backend-memcached-0.credit-backend-memcached',11211],
        ['credit-backend-memcached-1.credit-backend-memcached',11211]
    ],
];
