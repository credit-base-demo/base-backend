#!/usr/local/bin/php

<?php
use Marmot\Core;

use Common\Model\IApproveAble;

use Service\ServiceRequirement\Model\ServiceRequirement;
use Service\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;

include '../cron/ICron.php';

class serviceRequirementValidityPeriodCron implements ICron
{
    use printLogTrait;
    
    const PAGE = 0;
    const SIZE = 1000;
    
    public function fetchServiceRequirementList()
    {
        $filter = array();
        
        $filter['validityPeriod'] = time();
        $filter['applyStatus'] = IApproveAble::APPLY_STATUS['APPROVE'];
        $filter['status'] = ServiceRequirement::STATUS['NORMAL'];
        
        $serviceRequirementRepository = new ServiceRequirementRepository();
        list($serviceRequirementList, $count) = $serviceRequirementRepository->filter(
            $filter,
            array(),
            self::PAGE,
            self::SIZE
        );

       return [$serviceRequirementList, $count];
    }

    public function run()
    {
        $ids = array();
        list($serviceRequirementList, $count) = $this->fetchServiceRequirementList();

        $this->printLog("上架需求超过有效期自动关闭,开始更新时间为 ".date('Y-m-d H:i:s').PHP_EOL);

        foreach ($serviceRequirementList as $serviceRequirement) {
            if ($serviceRequirement->close()) {
                $ids[] = $serviceRequirement->getId();
                $this->printLog("数据更新成功,成功id为 ".$serviceRequirement->getId().PHP_EOL);
            } else {
                $this->printLog("数据更新失败,失败id为 ".$serviceRequirement->getId().PHP_EOL);
            }
        }

        $this->printLog("上架需求超过有效期自动关闭,更新完成,结束时间为 ".date('Y-m-d H:i:s')."更新成功的数据总数为 ".count($ids).", 实际需要更新的数据总数为 ".$count.PHP_EOL);

        exit();
    }

    public function mock()
    {
        $ids = array();
        list($serviceRequirementList, $count) = $this->fetchServiceRequirementList();

        foreach ($serviceRequirementList as $serviceRequirement) {
            $ids[] = $serviceRequirement->getId();
        }
        
        $this->printLog("mock:上架需求超过有效期自动关闭,需要更新的需求id为".implode(',',$ids).PHP_EOL);
        
        exit();
    }
}
