<?php
namespace News\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class News implements IObject
{
    use Object;

    /**
     * @var CATEGORY['POLICIES_REGULATIONS']  政策法规 1
     * @var CATEGORY['CREDIT_DYNAMICS']  信用动态 2
     * @var CATEGORY['CREDIT_RESEARCH']  信用研究 3
     */
    const CATEGORY = array(
        'POLICIES_REGULATIONS' => 1,
        'CREDIT_DYNAMICS' => 2,
        'CREDIT_RESEARCH' => 3
    );
    const CATEGORY_CN = array(
        self::CATEGORY['POLICIES_REGULATIONS'] => '政策法规',
        self::CATEGORY['CREDIT_DYNAMICS'] => '信用动态',
        self::CATEGORY['CREDIT_RESEARCH'] => '信用研究'
    );

    /**
     * @var STATUS['ENABLED']  启用，默认 0
     * @var STATUS['DISABLED']  禁用 -2
     */
    const STATUS = array(
        'ENABLED' => 0,
        'DISABLED' => -2
    );
    /**
     * @var APPLY_STATUS['PENDING']  待审核，默认 0
     * @var APPLY_STATUS['APPROVED']  已通过 2
     * @var APPLY_STATUS['REJECTED']  已驳回 -2
     */
    const APPLY_STATUS = array(
        'PENDING' => 0,
        'APPROVED' => 2,
        'REJECTED' => -2
    );

    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $title 新闻标题
     */
    private $title;
    /**
     * @var string $source 新闻来源
     */
    private $source;
    /**
     * @var int $category 新闻分类
     */
    private $category;
    /**
     * @var int $applyStatus 审核状态
     */
    private $applyStatus;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->source = '';
        $this->category = 0;
        $this->status = self::STATUS['ENABLED'];
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->source);
        unset($this->category);
        unset($this->status);
        unset($this->applyStatus);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setSource(string $source) : void
    {
        $this->source = $source;
    }

    public function getSource() : string
    {
        return $this->source;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array(
            $category,
            self::CATEGORY
        ) ? $category : 0;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array(
            $status,
            self::STATUS
        ) ? $status : self::STATUS['ENABLED'];
    }

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = in_array(
            $applyStatus,
            self::APPLY_STATUS
        ) ? $applyStatus : self::APPLY_STATUS['PENDING'];
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }
}
