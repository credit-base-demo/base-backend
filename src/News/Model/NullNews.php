<?php
namespace News\Model;

use Marmot\Interfaces\INull;
use Marmot\Core;

class NullNews extends News implements INull
{
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
