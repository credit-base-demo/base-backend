<?php
namespace News\Adapter;

use News\Model\News;

/**
 * @codeCoverageIgnore
 */
interface INewsAdapter
{
    public function fetchOne($id) : News;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
