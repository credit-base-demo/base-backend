<?php
namespace News\Adapter;

use Marmot\Core;

use News\Model\News;
use News\Utils\ObjectGenerate;

/**
 * @codeCoverageIgnore
 */
class NewsMockAdapter implements INewsAdapter
{
    public function fetchOne($id) : News
    {
        return ObjectGenerate::generateNews($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4, 5];

        $newsList = array();

        foreach ($ids as $id) {
            $newsList[$id] = ObjectGenerate::generateNews($id);
        }

        return $newsList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4, 5];
        $count = 5;

        return array($this->fetchList($ids), $count);
    }
}
