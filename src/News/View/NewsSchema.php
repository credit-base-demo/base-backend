<?php
namespace News\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

use News\Model\News;

/**
 * @codeCoverageIgnore
 */
class NewsSchema extends SchemaProvider
{
    protected $resourceType = 'news';

    public function getId($news) : int
    {
        return $news->getId();
    }

    public function getAttributes($news) : array
    {
        return [
            'title' => $news->getTitle().'【'.News::CATEGORY_CN[$news->getCategory()].'】',
            'source' => $news->getSource(),
            'category' => $news->getCategory(),
            'categorCn' => News::CATEGORY_CN[$news->getCategory()],

            'status' => $news->getStatus(),
            'applyStatus' => $news->getApplyStatus(),
            'createTime' => $news->getCreateTime(),
            'updateTime' => $news->getUpdateTime(),
            'statusTime' => $news->getStatusTime(),
        ];
    }
}
