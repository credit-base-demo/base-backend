<?php
namespace News\Utils;

use News\Model\News;

class ObjectGenerate
{
    public static function generateNews(
        int $id = 0
    ) : News {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($id);

        $news = new News($id);

        $news->setId($id);

        //title
        $title = '（新闻标题）'.$faker->bank;
        $news->setTitle($title);
        //source
        $source = $faker->bank;
        $news->setSource($source);
        //category
        $category = $faker->randomElement(News::CATEGORY);
        $news->setCategory($category);

        //status
        $status = $faker->randomElement(News::STATUS);
        $news->setStatus($status);
        //applyStatus
        $applyStatus = $faker->randomElement(News::APPLY_STATUS);
        $news->setApplyStatus($applyStatus);

        $news->setCreateTime($faker->unixTime());
        $news->setUpdateTime($faker->unixTime());
        $news->setStatusTime($faker->unixTime());

        return $news;
    }
}
