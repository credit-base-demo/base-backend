<?php
namespace News\Repository;

use Marmot\Framework\Classes\Repository;

use News\Model\News;
use News\Adapter\INewsAdapter;
use News\Adapter\NewsMockAdapter;

class NewsRepository extends Repository implements INewsAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new NewsMockAdapter();
    }

    protected function getActualAdapter() : INewsAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : INewsAdapter
    {
        return $this->adapter;
    }

    public function fetchOne($id) : News
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
