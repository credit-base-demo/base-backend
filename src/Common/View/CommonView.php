<?php
namespace Common\View;

use Marmot\Interfaces\IView;

/**
 * @SuppressWarnings(PHPMD)
 */
abstract class CommonView implements IView
{
    use JsonApiView;

    private $rules;

    private $data;
    
    private $encodingParameters;

    public function __construct($data, $encodingParameters = null)
    {
        $this->data = $data;
        $this->encodingParameters = $encodingParameters;

        $this->rules = array(
            \Crew\Model\Crew::class => \Crew\View\CrewSchema::class,
            \Crew\Model\NullCrew::class => \Crew\View\CrewSchema::class,

            \News\Model\News::class => \News\View\NewsSchema::class,
            \News\Model\NullNews::class => \News\View\NewsSchema::class,
        );
    }
    
    public function display()
    {
        return $this->jsonApiFormat($this->data, $this->rules, $this->encodingParameters);
    }
}
