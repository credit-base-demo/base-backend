<?php
namespace Crew\Utils;

use Crew\Model\Crew;

class ObjectGenerate
{
    public static function generateCrew(
        int $id = 0
    ) : Crew {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($id);

        $crew = new Crew($id);

        $crew->setId($id);

        //userName
        $userName = $faker->name;
        $crew->setUserName($userName);
        //cellphone
        $cellphone = $faker->numerify('###########');
        $crew->setCellphone($cellphone);
        //purviews
        $purviews = $faker->randomElements(array(1,2,3,4), random_int(1, 4));
        $crew->setPurviews($purviews);

        $crew->setCreateTime($faker->unixTime());
        $crew->setUpdateTime($faker->unixTime());
        $crew->setStatusTime($faker->unixTime());

        return $crew;
    }
}
