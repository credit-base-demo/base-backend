<?php
namespace Crew\Translator;

use Marmot\Interfaces\ITranslator;

use Crew\Model\Crew;
use Crew\Model\NullCrew;

class CrewDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $crew = null)
    {
        if (!isset($expression['crew_id'])) {
            return NullCrew::getInstance();
        }

        if ($crew == null) {
            $crew = new Crew($expression['crew_id']);
        }
        $crew->setUserName($expression['user_name']);
        $crew->setCellphone($expression['cellphone']);
        $crew->setPurviews(json_decode($expression['purviews'], true));

        $crew->setStatus($expression['status']);
        $crew->setCreateTime($expression['create_time']);
        $crew->setUpdateTime($expression['update_time']);
        $crew->setStatusTime($expression['status_time']);

        return $crew;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array())
    {
        if (!$crew instanceof Crew) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'userName',
                'cellphone',
                'purviews',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['crew_id'] = $crew->getId();
        }
        if (in_array('userName', $keys)) {
            $expression['user_name'] = $crew->getUserName();
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $crew->getCellphone();
        }
        if (in_array('purviews', $keys)) {
            $expression['purviews'] = $crew->getPurviews();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $crew->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $crew->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $crew->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $crew->getStatusTime();
        }

        return $expression;
    }
}
