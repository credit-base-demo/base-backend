<?php
namespace Crew\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

/**
 * @codeCoverageIgnore
 */
class CrewSchema extends SchemaProvider
{
    protected $resourceType = 'crews';

    public function getId($crew) : int
    {
        return $crew->getId();
    }

    public function getAttributes($crew) : array
    {
        return [
            'userName' => $crew->getUserName(),
            'cellphone' => $crew->getCellphone(),
            'purviews' => $crew->getPurviews(),

            'status' => $crew->getStatus(),
            'createTime' => $crew->getCreateTime(),
            'updateTime' => $crew->getUpdateTime(),
            'statusTime' => $crew->getStatusTime(),
        ];
    }
}
