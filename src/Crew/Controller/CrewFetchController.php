<?php
namespace Crew\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IFetchController;

use Crew\Repository\CrewRepository;
use Crew\View\CrewView;

class CrewFetchController extends Controller implements IFetchController
{
    use JsonApiTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    protected function getRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    public function fetchOne(int $id)
    {
        $crew = $this->getRepository()->fetchOne($id);

        if (!$crew instanceof INull) {
            $this->renderView(new CrewView($crew));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function fetchList(string $ids)
    {
        $ids = explode(',', $ids);

        $crewList = array();

        $crewList = $this->getRepository()->fetchList($ids);

        if (!empty($crewList)) {
            $this->renderView(new CrewView($crewList));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        list($crewList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = new CrewView($crewList);
            $view->pagination(
                'crews',
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
