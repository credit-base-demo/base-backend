<?php
namespace Crew\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Classes\CommandBus;

use Crew\Repository\CrewRepository;
use Crew\View\CrewView;
use Crew\Model\Crew;
use Crew\Command\Crew\ConfigurePurviewCommand;
use Crew\CommandHandler\Crew\CrewCommandHandlerFactory;

class ConfigurePurviewController extends Controller
{
    use JsonApiTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new CrewCommandHandlerFactory());
    }

    protected function getRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    public function configurePurviews(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        
        $purviews = $attributes['purviews'];

        $commandBus = $this->getCommandBus();

        $command = new ConfigurePurviewCommand(
            $purviews,
            $id
        );

        if ($commandBus->send($command)) {
            $repository = $this->getRepository();
            $crew = $repository->fetchOne($id);
            if ($crew instanceof Crew) {
                $this->render(new CrewView($crew));
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
