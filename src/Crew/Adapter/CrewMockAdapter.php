<?php
namespace Crew\Adapter;

use Marmot\Core;

use Crew\Model\Crew;
use Crew\Utils\ObjectGenerate;

/**
 * @codeCoverageIgnore
 */
class CrewMockAdapter implements ICrewAdapter
{
    public function fetchOne($id) : Crew
    {
        return ObjectGenerate::generateCrew($id);
    }

    public function fetchList(array $ids) : array
    {
        $ids = [1, 2, 3, 4];

        $newsList = array();

        foreach ($ids as $id) {
            $newsList[$id] = ObjectGenerate::generateCrew($id);
        }

        return $newsList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function update(Crew $crew, array $keys = array()) : bool
    {
        unset($crew);
        unset($keys);
        return true;
    }
}
