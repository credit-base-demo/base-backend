<?php
namespace Crew\Adapter;

use Crew\Model\Crew;

/**
 * @codeCoverageIgnore
 */
interface ICrewAdapter
{
    public function fetchOne($id) : Crew;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function update(Crew $crew, array $keys = array()) : bool;
}
