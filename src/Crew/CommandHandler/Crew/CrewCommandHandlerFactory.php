<?php
namespace Crew\CommandHandler\Crew;

use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

class CrewCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Crew\Command\Crew\ConfigurePurviewCommand' =>
        'Crew\CommandHandler\Crew\ConfigurePurviewCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : new NullCommandHandler();
    }
}
