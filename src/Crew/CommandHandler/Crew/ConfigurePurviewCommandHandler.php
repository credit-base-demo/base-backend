<?php
namespace Crew\CommandHandler\Crew;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Crew\Command\Crew\ConfigurePurviewCommand;
use Crew\Repository\CrewRepository;

class ConfigurePurviewCommandHandler implements ICommandHandler
{
    protected function getRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof ConfigurePurviewCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getRepository();
        $crew = $repository->fetchOne($command->id);

        $crew->setPurviews($command->purviews);

        return $crew->configurePurviews();
    }
}
