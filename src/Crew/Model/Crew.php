<?php
namespace Crew\Model;

use Marmot\Core;

use User\Model\User;

use Crew\Repository\CrewRepository;

class Crew extends User
{

    /**
     * @var array $purviews 所属权限
     */
    private $purviews;

    private $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->purviews = array();
        $this->status = 0;
        $this->repository = new CrewRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->id);
        unset($this->purviews);
        unset($this->status);
        unset($this->repository);
    }

    public function setPurviews(array $purviews) : void
    {
        $this->purviews = $purviews;
    }

    public function getPurviews() : array
    {
        return $this->purviews;
    }

    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }

    public function configurePurviews() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->getRepository()->update(
            $this,
            array(
                'purviews',
                'updateTime'
            )
        );
    }
}
