<?php
namespace Crew\Command\Crew;

use Marmot\Interfaces\ICommand;

class ConfigurePurviewCommand implements ICommand
{
    /**
     * @var array $purviews 所属权限
     */
    public $purviews;

    public $id;

    public function __construct(
        array $purviews,
        int $id
    ) {
        $this->purviews = $purviews;
        $this->id = $id;
    }
}
