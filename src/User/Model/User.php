<?php
namespace User\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;

abstract class User implements IObject
{

    use Object;
    /**
     * @var int $id
     */
    protected $id;
    /**
     * @var string $userName 用户名预留字段
     */
    protected $userName;
    /**
     * @var string $cellphone 手机号
     */
    protected $cellphone;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->userName = '';
        $this->cellphone = '';
        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->userName);
        unset($this->cellphone);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setUserName(string $userName) : void
    {
        $this->userName = $userName;
    }

    public function getUserName() : string
    {
        return $this->userName;
    }

    public function setCellphone(string $cellphone) : void
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
