<?php
/**
 * \d+(,\d+)*
 * 路由设置
 */

return [
        //监控检测
        [
            'method'=>'GET',
            'rule'=>'/healthz',
            'controller'=>[
                'Home\Controller\HealthzController',
                'healthz'
            ]
        ],
        //获取单条人员
        // [
        //     'method'=>'GET',
        //     'rule'=>'/crews/{id:\d+}',
        //     'controller'=>[
        //         'Crew\Controller\CrewFetchController',
        //         'fetchOne'
        //     ]
        // ],
        // //配置权限
        // [
        //     'method'=>'PATCH',
        //     'rule'=>'/crews/{id:\d+}/configurePurview',
        //     'controller'=>[
        //         'Crew\Controller\ConfigurePurviewController',
        //         'configurePurviews'
        //     ]
        // ],
        // //获取单条新闻
        // [
        //     'method'=>'GET',
        //     'rule'=>'/news/{id:\d+}',
        //     'controller'=>[
        //         'News\Controller\NewsFetchController',
        //         'fetchOne'
        //     ]
        // ],
        // //获取多条新闻
        // [
        //     'method'=>'GET',
        //     'rule'=>'/news/{ids:\d+,[\d,]+}',
        //     'controller'=>[
        //         'News\Controller\NewsFetchController',
        //         'fetchList'
        //     ]
        // ],
        // //根据检索条件查询新闻
        // [
        //     'method'=>'GET',
        //     'rule'=>'/news',
        //     'controller'=>[
        //         'News\Controller\NewsFetchController',
        //         'filter'
        //     ]
        // ],
];
