<?php
namespace User\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class UserTest extends TestCase
{
    private $user;

    public function setUp()
    {
        $this->user = $this->getMockBuilder('User\Model\User')
                      ->getMockForAbstractClass();
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("Marmot\Common\Model\IObject", $this->user);
    }

    public function testUserConstructor()
    {
        $this->assertEquals(0, $this->user->getId());

        //测试初始化用户名预留字段
        $this->assertEmpty($this->user->getUserName());

        //测试初始化用户手机号
        $this->assertEmpty($this->user->getCellphone());

        //测试初始化注册时间
        $this->assertEquals(Core::$container->get('time'), $this->user->getCreateTime());

        //测试初始化更新时间
        $this->assertEquals(Core::$container->get('time'), $this->user->getUpdateTime());

        //测试初始化状态更新时间
        $this->assertEquals(0, $this->user->getStatusTime());

        //测试初始化状态
        $this->assertEquals(0, $this->user->getStatus());
    }

    public function testSetId()
    {
        $this->user->setId(1);
        $this->assertEquals(1, $this->user->getId());
    }

    //userName 测试 ---------------------------------------------------- start
    /**
     * 设置 User setUserName() 正确的传参类型,期望传值正确
     */
    public function testSetUserNameCorrectType()
    {
        $this->user->setUserName('string');
        $this->assertEquals('string', $this->user->getUserName());
    }

    /**
     * 设置 User setUserName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserNameWrongType()
    {
        $this->user->setUserName(array(1,2,3));
    }
    //userName 测试 ----------------------------------------------------   end

    //cellphone 测试 --------------------------------------------------- start
    /**
     * 设置 User setCellphone() 正确的传参类型,期望传值正确
     */
    public function testSetCellphoneCorrectType()
    {
        $this->user->setCellphone('18800000001');
        $this->assertEquals('18800000001', $this->user->getCellphone());
    }
    
    /**
     * 设置 User setCellphone() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCellphoneWrongType()
    {
        $this->user->setCellphone(array(1,2,3));
    }
    //cellphone 测试 ---------------------------------------------------   end
}
