<?php
namespace News\Controller;

use Marmot\Framework\Classes\Request;

use Prophecy\Argument;

use Common\Controller\FetchControllerTestCase;

use News\Repository\NewsRepository;
use News\Model\News;

class NewsFetchControllerTest extends FetchControllerTestCase
{
    protected $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(NewsFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'render',
                    'displayError',
                    'formatParameters'
                ]
            )->getMock();

        $this->request = $this->prophesize(Request::class);
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testFetchOne()
    {
        $this->dealFetchOne(
            [
                'repository' => NewsRepository::class,
                'model' => News::class,
                'controller' => NewsFetchController::class
            ],
            [
                'getRepository', 'renderView'
            ],
            true
        );
    }

    public function testFetchListSuccess()
    {
        $modelList = array($this->prophesize(News::class),
            $this->prophesize(News::class));
        $classList =  [
                        'repository' => NewsRepository::class,
                        'controller' => NewsFetchController::class
                    ];
        $mockMethods = ['getRepository', 'renderView'];

        $this->dealFetchListTrue($modelList, $classList, $mockMethods);
    }

    public function testFetchListFailure()
    {
        $classList =  [
                        'repository' => NewsRepository::class,
                        'controller' => NewsFetchController::class
                    ];
        $mockMethods = ['getRepository', 'displayError'];
         
        $this->dealFetchListFalse($classList, $mockMethods);
    }
}
