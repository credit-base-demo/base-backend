<?php
namespace News\Model;

use PHPUnit\Framework\TestCase;

class NewsTest extends TestCase
{
    private $stub;

    private $faker;

    public function setUp()
    {
        $this->stub = new News();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->faker);
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testNewsConstructor()
    {
        $this->assertEquals(0, $this->stub->getId());
        $this->assertEmpty($this->stub->getTitle());
        $this->assertEmpty($this->stub->getSource());
        $this->assertEquals(0, $this->stub->getCategory());
        $this->assertEquals(News::STATUS['ENABLED'], $this->stub->getStatus());
        $this->assertEquals(News::APPLY_STATUS['PENDING'], $this->stub->getApplyStatus());
    }

    //title 测试 ---------------------------------------------------- start
    /**
     * 设置 setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $title = $this->faker->name();
        $this->stub->setTitle($title);
        $this->assertEquals($title, $this->stub->getTitle());
    }

    /**
     * 设置 setTitle() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $title = array($this->faker->name());
        $this->stub->setTitle($title);
    }
    //title 测试 ----------------------------------------------------   end

    //source 测试 ---------------------------------------------------- start
    /**
     * 设置 setSource() 正确的传参类型,期望传值正确
     */
    public function testSetSourceCorrectType()
    {
        $source = $this->faker->name();
        $this->stub->setSource($source);
        $this->assertEquals($source, $this->stub->getSource());
    }

    /**
     * 设置 setSource() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceWrongType()
    {
        $source = array($this->faker->name());
        $this->stub->setSource($source);
    }
    //source 测试 ----------------------------------------------------   end

    //category 测试 ---------------------------------------------------- start
    /**
     * @dataProvider categoryDataProvider
     */
    public function testSetCategory($actural, $expected)
    {
        $this->stub->setCategory($actural);
        $result = $this->stub->getCategory();
        $this->assertEquals($expected, $result);
    }

    public function categoryDataProvider()
    {
        return [
            [News::CATEGORY['POLICIES_REGULATIONS'], News::CATEGORY['POLICIES_REGULATIONS']],
            [News::CATEGORY['CREDIT_DYNAMICS'], News::CATEGORY['CREDIT_DYNAMICS']],
            [News::CATEGORY['CREDIT_RESEARCH'], News::CATEGORY['CREDIT_RESEARCH']],
            [999, 0]
        ];
    }
    //category 测试 ----------------------------------------------------   end
}
