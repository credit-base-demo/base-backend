<?php

class MongodbApi implements ISmokeTest
{
    public function smokeTest() : bool
    {
        return mongodbSmokeTest(
            \Marmot\Core::$container->get('mongo.host'),
            \Marmot\Core::$container->get('mongo.uriOptions'),
            \Marmot\Core::$container->get('mongo.driverOptions')
        );
    }
}
