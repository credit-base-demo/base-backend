

CREATE TABLE `pcore_crew` (
  `crew_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '人员主键id',
  `user_name` varchar(255) NOT NULL COMMENT '用户名',
  `cellphone` char(11) NOT NULL COMMENT '手机号',
  `purviews` json NOT NULL COMMENT '权限',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  PRIMARY KEY (`crew_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `cellphone` (`cellphone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='人员表';

-- --------------------------------------------------------
