# 人员接口文档

---

## 简介

本文档主要描述人员模块相关接口请求及响应说明。

## 目录

* [参考文档](#参考文档)
    * [项目字典](#项目字典)
    * [接口错误返回说明](#接口错误返回说明) 
    * [内置人员数据](#内置人员数据)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
    * [获取单条数据](#获取单条数据)
    * [获取多条数据](#获取多条数据)
    * [根据检索条件查询数据](#根据检索条件查询数据)
    * [配置权限](#配置权限)
    * [接口返回示例](#接口返回示例)
        * [单条示例](#单条示例)
        * [多条示例](#多条示例)

## <a name="参考文档">参考文档</a>

* <a name="项目字典">项目字典</a>
    * [人员项目字典](./docs/Dictionary/crew.md "人员项目字典")
* <a name="接口错误返回说明">接口错误返回说明</a>
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明") 

## <a name="参数说明">参数说明</a>

<table>
  <tr>
    <th><b>英文名称</b></th>
    <th><b>类型</b></th>
    <th><b>请求参数是否必填</b></th>
    <th><b>示例</b></th>
    <th><b>描述</b></th>
  </tr> 
  <tr>
    <td>userName</td>
    <td>string</td>
    <td></td>
    <td>测试账号一</td>
    <td>用户名.唯一</td>
  </tr>
  <tr>
    <td>cellphone</td>
    <td>string</td>
    <td></td>
    <td>18800000001</td>
    <td>手机号.唯一</td>
  </tr>
  <tr>
    <td>purviews</td>
    <td>string</td>
    <td></td>
    <td>array(1,2,3)</td>
    <td>所属权限</td>
  </tr>
</table>

### <a name="内置人员数据">内置人员数据示例</a>

* 人员id为 `1`, 用户名为 `测试账号一`, 手机号为 `18800000001`.
* 人员id为 `2`, 用户名为 `测试账号二`, 手机号为 `18800000002`.
* 人员id为 `3`, 用户名为 `测试账号三`, 手机号为 `18800000003`.
* 人员id为 `4`, 用户名为 `测试账号四`, 手机号为 `18800000004`.

## <a name="接口示例">接口示例</a>

### 获取数据支持include、fields请求参数

```
1、include请求参数
2、fields[TYPE]请求参数
    2.1 fields[crews]
```

示例

```php
$response = $client->request('GET', 'crews/1?fields[crews]=userName,cellphone',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取单条数据">获取单条数据示例</a>

路由

```
通过GET传参
/crews/{id:\d+}
```

示例

```php
$response = $client->request('GET', 'crews/1',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取多条数据">获取多条数据示例</a>

路由

```
通过GET传参
/crews/{ids:\d+,[\d,]+}
```

示例

```php
$response = $client->request('GET', 'crews/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="根据检索条件查询数据">根据检索条件查询数据示例</a>

路由

```
通过GET传参
/crews
```

示例

```php
$response = $client->request('GET', 'crews',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="配置权限">配置权限示例</a>

路由

```
通过PATCH传参
/crews/{id:\d+}/configurePurview
```

示例

```php
$data = array("data"=>array(
                    "type"=>"crews",
                    "attributes"=>array(
                        "purviews"=>array(1,3)
                    )
                )
        );
$response = $client->request(
                'PATCH',
                'crews/1/configurePurview',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条示例">单条示例</a>

```
{
    "meta": [],
    "data": {
        "type": "crews",
        "id": "1",
        "attributes": {
            "userName": "测试账号一",
            "cellphone": "18800000001",
            "purviews": [1,3],
            "status": 0,
            "createTime": 1531897790,
            "updateTime": 1531897790,
            "statusTime": 0
        },
        "links": {
            "self": "127.0.0.1:8080\/crews\/1"
        }
    }
}
```

#### <a name="多条示例">多条示例</a>

```
{
    "meta": {
        "count": 4,
        "links": {
            "first": null,
            "last": null,
            "prev": null,
            "next": null
        }
    },
    "links": {
        "first": null,
        "last": null,
        "prev": null,
        "next": null
    },
    "data": [
        {
            "type": "crews",
            "id": "1",
            "attributes": {
                "userName": "\u6d4b\u8bd5\u8d26\u53f7\u4e00",
                "cellphone": "18800000001",
                "purviews": [
                    "1",
                    "4"
                ],
                "status": 0,
                "createTime": 1584687235,
                "updateTime": 1595848142,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8080\/crews\/1"
            }
        },
        {
            "type": "crews",
            "id": "2",
            "attributes": {
                "userName": "\u6d4b\u8bd5\u8d26\u53f7\u4e8c",
                "cellphone": "18800000002",
                "purviews": [],
                "status": 0,
                "createTime": 1584687235,
                "updateTime": 1584687235,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8080\/crews\/2"
            }
        },
        {
            "type": "crews",
            "id": "3",
            "attributes": {
                "userName": "\u6d4b\u8bd5\u8d26\u53f7\u4e09",
                "cellphone": "18800000003",
                "purviews": [],
                "status": 0,
                "createTime": 1584687235,
                "updateTime": 1584687235,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8080\/crews\/3"
            }
        },
        {
            "type": "crews",
            "id": "4",
            "attributes": {
                "userName": "\u6d4b\u8bd5\u8d26\u53f7\u56db",
                "cellphone": "18800000004",
                "purviews": [],
                "status": 0,
                "createTime": 1584687235,
                "updateTime": 1584687235,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8080\/crews\/4"
            }
        }
    ]
}
```