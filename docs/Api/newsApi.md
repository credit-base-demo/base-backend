# 新闻接口文档

---

## 简介

本文档主要描述新闻模块相关接口请求及响应说明。

## 目录

* [参考文档](#参考文档)
    * [项目字典](#项目字典)
    * [接口错误返回说明](#接口错误返回说明) 
* [参数说明](#参数说明)
* [接口示例](#接口示例)
    * [获取单条数据](#获取单条数据)
    * [获取多条数据](#获取多条数据)
    * [根据检索条件查询数据](#根据检索条件查询数据)
    * [接口返回示例](#接口返回示例)
        * [单条示例](#单条示例)
        * [多条示例](#多条示例)

## <a name="参考文档">参考文档</a>

* <a name="项目字典">项目字典</a>
    * [新闻项目字典](./docs/Dictionary/news.md "新闻项目字典")
* <a name="接口错误返回说明">接口错误返回说明</a>
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明") 

## <a name="参数说明">参数说明</a>

<table>
  <tr>
    <th><b>英文名称</b></th>
    <th><b>类型</b></th>
    <th><b>请求参数是否必填</b></th>
    <th><b>示例</b></th>
    <th><b>描述</b></th>
  </tr> 
  <tr>
    <td>title</td>
    <td>string</td>
    <td></td>
    <td>政策法规新闻标题</td>
    <td>标题</td>
  </tr>
  <tr>
    <td>source</td>
    <td>string</td>
    <td></td>
    <td>法院</td>
    <td>来源</td>
  </tr>
  <tr>
    <td>category</td>
    <td>int</td>
    <td></td>
    <td>1</td>
    <td>新闻分类.1(政策法规),2(信用动态),3(信用研究)</td>
  </tr>
  <tr>
    <td>categoryCn</td>
    <td>string</td>
    <td></td>
    <td>政策法规</td>
    <td>新闻分类中文</td>
  </tr>
  <tr>
    <td>status</td>
    <td>int</td>
    <td></td>
    <td>0</td>
    <td>状态.0(启用，默认),-2(禁用)</td>
  </tr>
  <tr>
    <td>applyStatus</td>
    <td>int</td>
    <td></td>
    <td>0</td>
    <td>审核状态.0(待审核，默认),2(已通过),-2(已驳回)</td>
  </tr>
</table>

## <a name="接口示例">接口示例</a>

### 获取数据支持include、fields请求参数

```
1、include请求参数
2、fields[TYPE]请求参数
    2.1 fields[news]
3、page请求参数
    3.1 page[number]=1 | 当前页
    3.2 page[size]=20 | 获取每页的数量
```

示例

```php
$response = $client->request('GET', 'news/1?fields[news]=source,title',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取单条数据">获取单条数据示例</a>

路由

```
通过GET传参
/news/{id:\d+}
```

示例

```php
$response = $client->request('GET', 'news/1',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取多条数据">获取多条数据示例</a>

路由

```
通过GET传参
/news/{ids:\d+,[\d,]+}
```

示例

```php
$response = $client->request('GET', 'news/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="根据检索条件查询数据">根据检索条件查询数据示例</a>

路由

```
通过GET传参
/news

1、检索条件
    1.1 filter[title] | 根据新闻标题搜索
    1.2 filter[category] | 根据新闻分类搜索 | 政策法规 1 |  信用动态 2 |  信用研究 3
2、排序
    2.1 sort=-id | -id 根据id倒序 | id 根据id正序
    2.2 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序
```

示例

```php
$response = $client->request('GET', 'news?filter[category]=1&sort=-id',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条示例">单条示例</a>

```
{
    "meta": [],
    "data": {
        "type": "news",
        "id": "1",
        "attributes": {
            "title": "政策法规新闻标题",
            "source": "法院",
            "category": 1,
            "categoryCn": "政策法规",
            "status": 0,
            "applyStatus": 0,
            "createTime": 1531897790,
            "updateTime": 1531897790,
            "statusTime": 0
        },
        "links": {
            "self": "127.0.0.1:8080\/news\/1"
        }
    }
}
```

#### <a name="多条示例">多条示例</a>

```
{
    "meta": {
        "count": 25,
        "links": {
            "first": 1,
            "last": 13,
            "prev": null,
            "next": 2
        }
    },
    "links": {
        "first": "127.0.0.1:8080\/news?page[number]=1&page[size]=2",
        "last": "127.0.0.1:8080\/news?page[number]=13&page[size]=2",
        "prev": null,
        "next": "127.0.0.1:8080\/news?page[number]=2&page[size]=2"
    },
    "data": [
        {
            "type": "news",
            "id": "1",
            "attributes": {
                "title": "政策法规新闻标题",
                "source": "法院",
                "category": 1,
                "categoryCn": "政策法规",
                "status": 0,
                "applyStatus": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8080\/news\/1"
            }
        },
        {
            "type": "news",
            "id": "2",
            "attributes": {
                "title": "信用动态新闻标题",
                "source": "发改委",
                "category": 2,
                "categoryCn": "信用动态",
                "status": 0,
                "applyStatus": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8080\/news\/2"
            }
        }
    ]
}
```

