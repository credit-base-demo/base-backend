# 新闻字典

### 英文名称

**中文名称** 描述信息

---

### title

**标题**

* string

### source

**来源**

* string

### category

**新闻分类**

* int
	* POLICIES_REGULATIONS => 1 | 政策法规
	* CREDIT_DYNAMICS => 2 | 信用动态
	* CREDIT_RESEARCH => 3 | 信用研究

### categoryCn

**新闻分类中文**

* string
	* 1 => 政策法规
	* 2 => 信用动态
	* 3 => 信用研究

### status

**状态**

* int
	* ENABLED => 0 | 启用，默认
	* DISABLED => -2 | 禁用

### applyStatus

**审核状态**

* int
	* PENDING => 0 | 待审核，默认
	* APPROVED => 2 | 已通过
	* REJECTED => -2 | 已驳回