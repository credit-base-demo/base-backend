# 通用项目字典

### 英文名称

**中文名称** 描述信息

---

## 动词

### show

**查看**

### add

**发布/新增**

### edit

**编辑**

### configurePurview 

**配置权限**

### verify

**审核**

### pending

**待审核**

### reject

**驳回**

### approve

**通过**  

### enable

**启用**

### disable

**禁用**