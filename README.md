# 公共业务基础项目后端服务

### 目录

* [简介](#abstract)
* [沟通记录](#communicationRecord)
* [项目字典](#dictionary)
* [控件规范](#widgetRule)
* [错误规范](#errorRule)
* [接口文档](#api)
* [参考文档](#tutor)
* [版本记录](#version)

---

### <a name="abstract">简介</a>

用于 信用平台2.0演示demo 公共业务基础项目 后端服务.

---

### <a name="communicationRecord">沟通记录</a>

根据沟通日期命名.

---

### <a name="dictionary">项目字典</a>

* 通用项目字典 `common`
	* [通用项目字典](./docs/Dictionary/common.md "通用项目字典")
* 人员项目字典 `crew`
	* [人员项目字典](./docs/Dictionary/crew.md "人员项目字典")
* 新闻项目字典 `news`
	* [新闻项目字典](./docs/Dictionary/news.md "新闻项目字典")

---

### <a name="widgetRule">控件规范</a>
	
---

### <a name="errorRule">错误规范</a>
	
---

### <a name="api">接口文档</a>
* 人员接口文档 `crew`
	* [人员接口文档](./docs/Api/crewApi.md "人员接口文档")
* 新闻接口文档 `news`
	* [新闻接口文档](./docs/Api/newsApi.md "新闻接口文档")

---

### <a name="tutor">参考文档</a>

* [jsonapi媒体协议](http://jsonapi.org/ "jsonapi")
* [框架](https://github.com/chloroplast1983/marmot) 

---

### <a name="version">版本记录</a>

* [0.1.0](./docs/Version/0.1.0.md "0.1.0")

